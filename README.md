# node-basic-info

Created with Node.js as part of [The Odin Project Curriculum](https://www.theodinproject.com/courses/nodejs/lessons/basic-informational-site).
This project was a tool for me to practice with Node concepts such as the http Module and the File System Module.

This was refactored to use Express.
